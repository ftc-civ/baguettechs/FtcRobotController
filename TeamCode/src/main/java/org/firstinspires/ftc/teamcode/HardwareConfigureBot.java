/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;

/**
 * This is NOT an opmode.
 *
 * This class can be used to define all the specific hardware for our robot.
 *
 * This hardware class assumes the following device names have been configured on the robot:
 * Note:  All names are lower case and some have single spaces between words.
 *
 * Motor channel:  front Left drive motor:        "frontLeft"
 * Motor channel:  front Right drive motor:        "frontRight"
 * Motor channel:  back Left drive motor:  "backLeft"
 * Motor channel:  back Right drive motor:  "backRight"
 * Webcam channel: webcam Vuforia : "Webcam 1"
 * Color Sensor channel: capteur de couleur : "ColorSensor"
 */
public class HardwareConfigureBot
{
    /* Public OpMode members. */
    public DcMotorEx frontLeftMotor = null;
    public DcMotorEx backLeftMotor = null;
    public DcMotorEx frontRightMotor = null;
    public DcMotorEx backRightMotor = null;
    public DcMotor collectorMotor = null;
    public DcMotor rampeMotor = null;
    public Servo wobbleServo= null;
    public static final double INIT_SERVO       =  0.75 ;


//    public static final double MID_SERVO       =  0.5 ;
//    public static final double ARM_UP_POWER    =  0.45 ;
//    public static final double ARM_DOWN_POWER  = -0.45 ;

    /*
     * IMPORTANT: You need to obtain your own license key to use Vuforia. The string below with which
     * 'parameters.vuforiaLicenseKey' is initialized is for illustration only, and will not function.
     * A Vuforia 'Development' license key, can be obtained free of charge from the Vuforia developer
     * web site at https://developer.vuforia.com/license-manager.
     *
     * Vuforia license keys are always 380 characters long, and look as if they contain mostly
     * random data. As an example, here is a example of a fragment of a valid key:
     *      ... yIgIzTqZ4mWjk9wd3cZO9T1axEqzuhxoGlfOOI2dRzKS4T0hQ8kT ...
     * Once you've obtained a license key, copy the string from the Vuforia web site
     * and paste it in to your code on the next line, between the double quotes.
     */
    public static final String VUFORIA_KEY =
            "AV6C5jj/////AAABmRYAyzGUZkbejyLfHAMMOYAgm6zoHkv+bfDX9sJL/plz8XEyXfNd07ViBIC8cNHvYHtapiTbjys2TJ9vXwPrM0iPwxKBH+2tObEAcA5B20NfCcj6SEB3ztH9dsYpffJ3Aoj8c8jcAydgAMD3OP70B4utix06Rb7Dcid9txXrsWktkJkVe8Xkmy1k1ySq+CMzgBCf1VrZtgW+pdEQCf3QYjEbeE9REWXKx8EScUlWQqmsquEXl1ZaCM2Aiqtgybg53VpmWqyVvFWNcuhd91IHkQbpVpKOiNQco1gDaLXA8YbuYZq+kVLeNshaQat28qF83Vu8XKoTr2QkrTpUzdbWXxh1nwSEEahwytUlilxUptfR";

    /* local OpMode members. */
    HardwareMap hardwareMap           =  null;
    private ElapsedTime period  = new ElapsedTime();
    // Déclaration de l'objet classCamera de la classe ClassCamera qui utilise la camera en mode détection des anneaux
    ClassCamera webcam;
    // Déclaration de l'objet classCouleur de la classe ClassCouleur qui utilise le capteur de couleur en mode capteur de luminosité
    ClassCouleur capteurCouleur;

    /* Constructor */
    public HardwareConfigureBot(){

    }

    /* Initialize standard Hardware interfaces */
    public void init(HardwareMap ahwMap, Telemetry telemetry, boolean encoder) {
        // Save reference to Hardware map
        hardwareMap = ahwMap;

        // On utilise le constructeur de ClassCamera pour construire notre objet classCamera correspondant à notre webcam
        // On fournit en argument l'abjet webcam et les objets nécessaires pour affichage sur l'écran (je crois) dont telemetry
        webcam = new ClassCamera(hardwareMap.get(WebcamName.class, "Webcam 1"), hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName()), telemetry);

        // On utilise le constructeur de ClassCouleur pour construire notre objet classCouleur correspondant au capteur
        // On fournit en argument la configuration harware et du capteur et telemetry
        capteurCouleur = new ClassCouleur(hardwareMap.get(NormalizedColorSensor.class, "ColorSensorV2"), telemetry);

        // Define and Initialize Motors
        frontLeftMotor = hardwareMap.get(DcMotorEx.class, "frontLeft");
        backLeftMotor = hardwareMap.get(DcMotorEx.class, "backLeft");
        frontRightMotor = hardwareMap.get(DcMotorEx.class, "frontRight");
        backRightMotor = hardwareMap.get(DcMotorEx.class, "backRight");
        collectorMotor = hardwareMap.get(DcMotor.class, "collector");
        rampeMotor = hardwareMap.get(DcMotor.class, "rampe");


        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery
        frontLeftMotor.setDirection(DcMotor.Direction.FORWARD);
        backLeftMotor.setDirection(DcMotor.Direction.FORWARD);
        frontRightMotor.setDirection(DcMotor.Direction.REVERSE);
        backRightMotor.setDirection(DcMotor.Direction.REVERSE);

        rampeMotor.setDirection(DcMotor.Direction.REVERSE);

        // Set all motors to zero power
        frontLeftMotor.setPower(0);
        backLeftMotor.setPower(0);
        frontRightMotor.setPower(0);
        backRightMotor.setPower(0);

        // Define and initialize ALL installed servos.
        wobbleServo  = hardwareMap.get(Servo.class, "WobbleMotor");
        //wobbleServo.setDirection(Servo.Direction.REVERSE);
        wobbleServo.setPosition(INIT_SERVO);

        // Set all motors to run without encoders.
        // May want to use RUN_USING_ENCODERS if encoders are installed.
        if (encoder == false) {
            frontLeftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            backLeftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            frontRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            backRightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        }
        else{
            frontLeftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            backLeftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            frontRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            backRightMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            
            frontLeftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            backLeftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            frontRightMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            backRightMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            // Send telemetry message to indicate successful Encoder reset
            telemetry.addData("Position Départ | ",  " Avant Gauche : %7d, Avant Droit : %7d, Arrière Gauche ! %7d, Arrière Droit : %7d",
                    frontLeftMotor.getCurrentPosition(),
                    frontRightMotor.getCurrentPosition(),
                    backLeftMotor.getCurrentPosition(),
                    backRightMotor.getCurrentPosition());
            telemetry.update();
        }
        // Define and initialize ALL installed servos.
//        leftClaw  = hwMap.get(Servo.class, "left_hand");
//        rightClaw = hwMap.get(Servo.class, "right_hand");
//        leftClaw.setPosition(MID_SERVO);
//        rightClaw.setPosition(MID_SERVO);
    }
 }

