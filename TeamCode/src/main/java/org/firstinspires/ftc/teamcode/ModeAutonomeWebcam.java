/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Ceci est notre premier essai de programme autonome.
 * Pour le moment, le but est d'être capable de détecter laprésence de 0, 1 ou 4 anneaux
 * et de éclencher une action différente selon les cas.
 * On peut également utiliser le capteur lumineux pour s'arrêter à la ligne blanche si besoin.
 */

@Autonomous(name="Autonome Detection Anneaux", group="Linear Opmode")
public class ModeAutonomeWebcam extends LinearOpMode {

    // Declare les moteurs et un chrono général.
    private ElapsedTime runtime = new ElapsedTime();

    HardwareConfigureBot robot  = new HardwareConfigureBot();   // Configuration harware du robot

    // Déclaration de l'objet classCamera de la classe ClassCamera qui utilise la camera en mode détection des anneaux
    ClassCamera classCamera;
    // Déclaration de l'objet classCouleur de la classe ClassCouleur qui utilise le capteur de couleur en mode capteur de luminosité
    ClassCouleur classCouleur;
    // Déclaration de l'objet move de la classe ClassMove qui permet de faire bouger le robot
    ClassMove move;

    @Override
    public void runOpMode() {


        // hsvValues contiendra les valeurs (hue,saturation,value) fournies par le capteur de couleur
        final float[] hsvValues = new float[3];

        // Ce booléen doit passer à vrai dès qu'un anneau est détecté pendant la phase d'approche
        Boolean objectDetected = false;
        // Ce booléen doit passer à vrai dès qu'un anneau est détecté pendant la phase d'approche
        Boolean lineDetected = false;

        move = new ClassMove(robot);

        robot.init(hardwareMap, telemetry, false);
        //robot.wobbleServo.setPosition(0.75);

        //Fin de la phase d'initialisation
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        // Abort this loop is started or stopped.
        while (!(isStarted() || isStopRequested())) {
            idle();
        }

        // Démarrage de la phase autonome
        runtime.reset();
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.update();
        ClassCamera.Detections ring = null;


        // Ces 3 variables permettent de définir le déplacement du robot, elles varient entre +1 et -1
        double drive = -0.3; // >0 vers l'avant et <0 vers l'arrière
        double strafe = 0; // >0 pour déplacement latéral à droite et <0 pour déplacement latéral à gauche
        double twist = 0; // >0 pour rotation dans le sens horaire vu du dessus et <0 pour rotation dans le sens anti-horaire
//        move.puissanceMoteurs(drive, strafe,twist,telemetry);

        ring = robot.webcam.detectRing();
        // objectDetected passe à vrai si quelque chose a été détecté
        if (ring != ClassCamera.Detections.NONE) {
            objectDetected = true;
        }

        // Première phase, on avance tant qu'on a pas vu un anneau ou la ligne blanche ou appui sur stop
        while (opModeIsActive() && !objectDetected && !robot.capteurCouleur.detectLigne("white")) {
            // Tentative de détection d'anneaux
            move.puissanceMoteurs(drive,strafe,twist,telemetry);
            ring = robot.webcam.detectRing();
            // objectDetected passe à vrai si quelque chose a été détecté
            if (ring != ClassCamera.Detections.NONE) {
                objectDetected = true;
            }
        }
        // Stop all motors
        robot.webcam.deactivate();
        robot.frontLeftMotor.setPower(0);
        robot.backLeftMotor.setPower(0);
        robot.frontRightMotor.setPower(0);
        robot.backRightMotor.setPower(0);
        // On attend 2 secondes
        sleep(1000);

        // On détecte encore une fois l'objet
        //ClassCamera.Detections ring = classCamera.detectRing();
        // On réalise une action différente en fonction de ce qui est détecté
        if (ring == ClassCamera.Detections.QUAD) {
            // On affiche ce qui a été détecté
            telemetry.addData("4 anneaux", "");
            telemetry.update();
            // On va dans la zone C
            drive = 0.0;
            strafe = -0.3;
            twist = 0.0;
            move.puissanceMoteurs(drive, strafe,twist,telemetry); // On se colle au mur de droite.
            sleep(3000);
            move.puissanceMoteurs(drive, -strafe,twist,telemetry); // On se décolle un peu du mur.
            sleep(500);
            drive = -0.4;
            strafe = 0.0;
            twist = 0.0;
            move.puissanceMoteurs(drive, strafe,twist,telemetry); // On avance tout droit.
            for (int i = 0; i < 3 ; i++) { // On veut aller après la 3ème ligne rouge.
                while (!robot.capteurCouleur.detectLigne("red")) { // Détection de la ligne rouge.
                }
                telemetry.addData("Ligne Rouge", "");
                telemetry.update();
                if (i != 2){ // Après les 2 première lignes rouge, on continue d'avancer un peu pour dépasser la ligne avant de recommencer à la détecter.
                    sleep(1000);
                }
            }
            move.puissanceMoteurs(0,0,0,telemetry);
            robot.wobbleServo.setPosition(0.9); // On baisse le bras pour libérer le Wobble.
            move.puissanceMoteurs(0,0.3,0,telemetry); // On de déplace à gauche pour se dégager du Wobble et se mettre en face du goal.
            sleep(2000);
            robot.wobbleServo.setPosition(0.5); // On range le bras du Wobble.
            move.puissanceMoteurs(-0.4,0,0,telemetry); // On avance jusqu'au goal.
            sleep(1000);
            robot.collectorMotor.setPower(-1);
            robot.rampeMotor.setPower(-1); // On met les anneaux dans le goal.
            sleep(5000);
            robot.collectorMotor.setPower(0); // On stoppe la rampe.
            robot.rampeMotor.setPower(0);
            move.puissanceMoteurs(+0.3, 0,0.0,telemetry); // On repart vers la ligne blanche.
            while (!robot.capteurCouleur.detectLigne("white")) {
            }
            move.puissanceMoteurs(+0.0, 0.0,0.0,telemetry); // On s'arête sur la ligne blanche.

        } else if (ring == ClassCamera.Detections.SINGLE) {
            telemetry.addData("1 anneau", "");
            telemetry.update();
            //On le fait bouger à droite jusqu'au mur.
            drive = 0.0;
            strafe = -0.3;
            twist = 0.0;
            move.puissanceMoteurs(drive, strafe,twist,telemetry);
            sleep(3000);
            move.puissanceMoteurs(drive, -strafe,twist,telemetry); // On s'écarte du mur.
            sleep(500);
            move.puissanceMoteurs(0.0, -0.0,0.0,telemetry);
            // On va dans la zone B
            drive = -0.4;
            strafe = 0.0;
            twist = 0.0;
            move.puissanceMoteurs(drive, strafe,twist,telemetry); // On avance.
            for (int i = 0; i < 2 ; i++) { // Jusqu'à la 2ème ligne rouge.
                while (!robot.capteurCouleur.detectLigne("red")) {
                }
                sleep(200);
            }
            move.puissanceMoteurs(0.0, -0.0,0.0,telemetry);
            sleep(500);
            move.puissanceMoteurs(0.0, 0.4,0.0,telemetry); // On part vers la gauche.
            for (int i = 0; i < 2 ; i++) {
                while (!robot.capteurCouleur.detectLigne("red")) {
                }
                move.puissanceMoteurs(0,0,0,telemetry);
                move.puissanceMoteurs(0.0, 0.4,0.0,telemetry);
                sleep(1000);
                robot.wobbleServo.setPosition(0.9); // On lâche le Wobble 1 seconde après avoir vu la première ligne rouge.
            }
            move.puissanceMoteurs(0.0, -0.0,0.0,telemetry);
            robot.wobbleServo.setPosition(0.5); // On range le bras Wobble.
            sleep(500);
            move.puissanceMoteurs(-0.4,0,0,telemetry); // On avance jusqu'au mur du goal.
            sleep(2000);
            move.puissanceMoteurs(0,-0.4,0,telemetry); // On se décale à droite jusque devant le mur.
            sleep(1500);
            move.puissanceMoteurs(0,0,0,telemetry);
            robot.collectorMotor.setPower(-1); // On met les anneaux.
            robot.rampeMotor.setPower(-1);
            sleep(5000);
            robot.collectorMotor.setPower(0); // On stoppe la rampe.
            robot.rampeMotor.setPower(0);
            move.puissanceMoteurs(0.4,-0,0,telemetry); // On se décale du mur.
            sleep(500);
            move.puissanceMoteurs(0,-0.5,0,telemetry); // On va a droite jusqu'au mur.
            sleep(2000);
            move.puissanceMoteurs(+0.5, 0,0.0,telemetry); // On part vers la ligne blanche.
            while (!robot.capteurCouleur.detectLigne("white")) {
            }
            move.puissanceMoteurs(+0.0, 0.0,0.0,telemetry); // On s'arrête dès qu'on a vu la liogne blanche.

        } else if (ring == ClassCamera.Detections.NONE) {
            telemetry.addData("Pas d'anneau", "");
            telemetry.update();
            // On va dans la zone A sachant qu'on part de la ligne blanche après n'avoir détecté aucun anneau.
            drive = 0.0;
            strafe = -0.3;
            twist = 0.0;
            move.puissanceMoteurs(drive, strafe,twist,telemetry); // On se décale jusqu'au mur.
            sleep(3000);
            drive = 0.3;
            strafe = 0.0;
            twist = 0.0;
            move.puissanceMoteurs(drive, strafe,twist,telemetry); // On recule jusqu'à la première ligne rouge.
            while (!robot.capteurCouleur.detectLigne("red")) {
            }
            move.puissanceMoteurs(0.0, -0.0,0.0,telemetry);
            robot.wobbleServo.setPosition(0.9); // On lâche le wobble.
            move.puissanceMoteurs(0.0, +0.3,0.0,telemetry); // On se décale à gauche pour libérer le Wobble.
            sleep(3000);
            robot.wobbleServo.setPosition(0.5); // On range le bras Wobble.
            move.puissanceMoteurs(-0.5, 0,0.0,telemetry); // On avance jusqu'au mur du goal.
            sleep(4000);
            robot.collectorMotor.setPower(-1); // On met les anneaux dans le goal.
            robot.rampeMotor.setPower(-1);
            sleep(5000);
            robot.collectorMotor.setPower(0); // On stoppe la rampe.
            robot.rampeMotor.setPower(0);
            move.puissanceMoteurs(+0.4, 0,0.0,telemetry); // On repart vers la ligne blanche.
            while (!robot.capteurCouleur.detectLigne("white")) {
            }
            move.puissanceMoteurs(+0.0, 0.0,0.0,telemetry); // On s'arrête sur la ligne blanche.
        }

        // Stop all motors
        robot.frontLeftMotor.setPower(0);
        robot.backLeftMotor.setPower(0);
        robot.frontRightMotor.setPower(0);
        robot.backRightMotor.setPower(0);


    }
}
