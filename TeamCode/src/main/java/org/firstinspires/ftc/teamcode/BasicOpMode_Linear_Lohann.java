/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="Basic: Linear OpMode Lohann", group="Linear Opmode")
//@Disabled
public class BasicOpMode_Linear_Lohann extends LinearOpMode {

    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();

    HardwareConfigureBot robot  = new HardwareConfigureBot();   // Configuration hardware du robot
    

// Déclaration de l'objet move de la classe ClassMove qui permet de faire bouger le robot
    ClassMove move;

    /*
    private DcMotor frontLeftMotor = null;
    private DcMotor backLeftMotor = null;
    private DcMotor frontRightMotor = null;
    private DcMotor backRightMotor = null;
     */
    //private DcMotor collectorMotor = null;
    //private DcMotor rampeMotor = null;
    private boolean collectorOn = false;
    private boolean rampeOn = false;
    private boolean b1Pressed = false;
    private boolean b2Pressed = false;


    @Override
    public void runOpMode() {

        move = new ClassMove(robot);

        robot.init(hardwareMap, telemetry, true);

        telemetry.addData("Status", "Initialized");
        telemetry.update();
        //fin de l'initalization. 

        // Wait for the game to start (driver presses PLAY)
        // Abort this loop is started or stopped.
        while (!(isStarted() || isStopRequested())) {
            idle();
        }

        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Setup a variable for each drive wheel to save power level for telemetry
            double drive = -gamepad1.left_stick_y; // Remember, this is reversed!
            double strafe = gamepad1.left_stick_x*1.5;
            double twist = gamepad1.right_stick_x;


            // Calcul des puissances à fournir à chaque moteur en fonction des valeurs x, y et rx reçues du gamepad
            // Put powers in the range of -1 to 1 only if they aren't already (not
            // checking would cause us to always drive at full speed

            //setting power to motors
            move.puissanceMoteurs(drive, strafe,twist,telemetry);

            // Utilisation de right_bumper pour le collecteur
            if (gamepad1.right_bumper && !b1Pressed) {
                b1Pressed = true;
                if (collectorOn == false) {
                    robot.collectorMotor.setPower(-1);
                    collectorOn = true;
                } else {
                    robot.collectorMotor.setPower(0);
                    collectorOn = false;
                }
            }
             if (!gamepad1.right_bumper) {
                b1Pressed = false;
             }
             // Utilisation de left_bumper pour la rampe
            if (gamepad1.left_bumper && !b2Pressed) {
                b2Pressed = true;
                if (rampeOn == false) {
                    robot.rampeMotor.setPower(-1);
                    rampeOn = true;
                } else {
                    robot.rampeMotor.setPower(0);
                    rampeOn = false;
                }
            }
            if (!gamepad1.left_bumper) {
                b2Pressed = false;
            }
            // Pilotage du bras pour le wobble avec bouton a et b
            if (gamepad1.b){
                robot.wobbleServo.setPosition(0.5);
            }
            if (gamepad1.a){
                robot.wobbleServo.setPosition(0.75);
            }
             // Show the elapsed game time and wheel power.
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.addData("Position Courante | ",  " Avant Gauche : %7d, Avant Droit : %7d, Arrière Gauche ! %7d, Arrière Droit : %7d",
                    robot.frontLeftMotor.getCurrentPosition(),
                    robot.frontRightMotor.getCurrentPosition(),
                    robot.backLeftMotor.getCurrentPosition(),
                    robot.backRightMotor.getCurrentPosition());
            telemetry.update();

            telemetry.addData("Valeurs gamepad ", "Drive (%.2f), Strafe (%.2f), Twist (%.2f)", drive, strafe, twist);
            telemetry.update();
        }
    }
}
