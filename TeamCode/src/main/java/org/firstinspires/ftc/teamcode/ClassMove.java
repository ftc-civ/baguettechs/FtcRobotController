/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/*
 * Classe pour faire bouger le robot.
 */

public class ClassMove {

    // Initialisation des variables
    private HardwareConfigureBot robot;

    ClassMove(HardwareConfigureBot globalRobot) {
        robot=globalRobot;
    }

    public void puissanceMoteurs (double drive, double strafe, double twist, Telemetry globalTelemetry) {
        // Calcul des puissances à fournir aux 4 moteurs en fonction des valeurs de drive, strafe et twist
        globalTelemetry.addData("Valeurs moteurs ", "Drive (%.2f), Strafe (%.2f), Twist (%.2f)", drive, strafe, twist);
        globalTelemetry.update();

        double frontLeftPower = drive + strafe + twist;
        double backLeftPower = drive - strafe + twist;
        double frontRightPower = drive - strafe - twist;
        double backRightPower = drive + strafe - twist;

        // Put powers in the range of -1 to 1 only if they aren't already (not
        // checking would cause us to always drive at full speed)

        if (Math.abs(frontLeftPower) > 1 || Math.abs(backLeftPower) > 1 ||
                Math.abs(frontRightPower) > 1 || Math.abs(backRightPower) > 1 ) {
            // Find the largest power
            double max = 0;
            max = Math.max(Math.abs(frontLeftPower), Math.abs(backLeftPower));
            max = Math.max(Math.abs(frontRightPower), max);
            max = Math.max(Math.abs(backRightPower), max);

            // Divide everything by max (it's positive so we don't need to worry
            // about signs)
            frontLeftPower /= max;
            backLeftPower /= max;
            frontRightPower /= max;
            backRightPower /= max;
        }
        
        if(Math.abs(frontLeftPower) > 0.1 && Math.abs(frontLeftPower) < 0.3){
            frontLeftPower += 0.2;
        }
        if(Math.abs(backLeftPower) > 0.1 && Math.abs(backLeftPower) < 0.3){
            backLeftPower += 0.2;
        }
        if(Math.abs(frontRightPower) > 0.1 && Math.abs(frontRightPower) < 0.3){
            frontRightPower += 0.2;
        }
        if(Math.abs(backRightPower) > 0.1 && Math.abs(backRightPower) < 0.3){
            backRightPower += 0.2;
        }


        // Réglage de la puissance voulue pour les moteurs
        robot.frontLeftMotor.setPower(frontLeftPower);
        robot.backLeftMotor.setPower(backLeftPower);
        robot.frontRightMotor.setPower(frontRightPower);
        robot.backRightMotor.setPower(backRightPower);
    }

}
