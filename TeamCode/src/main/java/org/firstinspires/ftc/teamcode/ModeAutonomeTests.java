/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Ceci est notre premier essai de programme autonome.
 * Pour le moment, le but est d'être capable de détecter laprésence de 0, 1 ou 4 anneaux
 * et de éclencher une action différente selon les cas.
 * On peut également utiliser le capteur lumineux pour s'arrêter à la ligne blanche si besoin.
 */

@Autonomous(name="Autonome Tests", group="Linear Opmode")
public class ModeAutonomeTests extends LinearOpMode {

    // Declare les moteurs et un chrono général.
    private ElapsedTime runtime = new ElapsedTime();

    HardwareConfigureBot robot  = new HardwareConfigureBot();   // Configuration harware du robot

    // Déclaration de l'objet classCouleur de la classe ClassCouleur qui utilise le capteur de couleur en mode capteur de luminosité
    ClassCouleur classCouleur;
    // Déclaration de l'objet move de la classe ClassMove qui permet de faire bouger le robot
    ClassMove move;

    @Override
    public void runOpMode() {


        // hsvValues contiendra les valeurs (hue,saturation,value) fournies par le capteur de couleur
        final float[] hsvValues = new float[3];

        // Ce booléen doit passer à vrai dès qu'un anneau est détecté pendant la phase d'approche
        Boolean objectDetected = false;
        // Ce booléen doit passer à vrai dès qu'un anneau est détecté pendant la phase d'approche
        Boolean lineDetected = false;

        move = new ClassMove(robot);

        robot.init(hardwareMap, telemetry, false);

        //Fin de la phase d'initialisation
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        // Abort this loop is started or stopped.
        while (!(isStarted() || isStopRequested())) {
            idle();
        }

        // Démarrage de la phase autonome
        runtime.reset();
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.update();

        // Ces 3 variables permettent de définir le déplacement du robot, elles varient entre +1 et -1
        double drive = -0.3; // >0 vers l'avant et <0 vers l'arrière
        double strafe = 0; // >0 pour déplacement latéral à droite et <0 pour déplacement latéral à gauche
        double twist = 0; // >0 pour rotation dans le sens horaire vu du dessus et <0 pour rotation dans le sens anti-horaire
        double duree = 0;
        double nb_lignes=0;

        /*while (!isStopRequested()) {
            move.puissanceMoteurs(drive, strafe,twist,telemetry);
            sleep(100);
            move.puissanceMoteurs(0, 0, 0, telemetry);
            robot.capteurCouleur.detectLigne("red");
            sleep(2000);
        }*/

        move.puissanceMoteurs(drive,strafe,twist,telemetry);
        double tmp = getRuntime();
        for (int i = 0; i < 3 ; i++) {
            while (!robot.capteurCouleur.detectLigne("red")) {
                duree=getRuntime()-tmp;
                telemetry.addData("Durée boucle :",duree);
                telemetry.update();
                tmp=getRuntime();
            }
            nb_lignes=nb_lignes+1;
            move.puissanceMoteurs(0, 0, 0, telemetry);
            //telemetry.addData("Ligne Rouge", "");
            //telemetry.addData("Nombre de lignes détectées :", nb_lignes);
            //telemetry.update();
            sleep(3000);
            move.puissanceMoteurs(drive, strafe,twist, telemetry);
            sleep(500);
        }


        // Stop all motors
        robot.frontLeftMotor.setPower(0);
        robot.backLeftMotor.setPower(0);
        robot.frontRightMotor.setPower(0);
        robot.backRightMotor.setPower(0);

    }
}